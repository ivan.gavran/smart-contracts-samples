var HypeContract = artifacts.require("./HypeContract.sol");
var AntiHypeContract = artifacts.require("./AntiHypeContract.sol");
var ContractB1 = artifacts.require("./ContractB1.sol");
var ContractB2 = artifacts.require("./ContractB2.sol");

var ContractToCheck = artifacts.require("./ContractToCheck.sol");
var Checker = artifacts.require("./Checker.sol");

module.exports = function(deployer) 
{
  deployer.deploy(HypeContract);
  deployer.deploy(AntiHypeContract);
//  deployer.link(HypeContract, AntiHypeContract);
  deployer.deploy(ContractB1);
  deployer.deploy(ContractB2);
	
	deployer.deploy(Checker);
	deployer.deploy(ContractToCheck);
};
