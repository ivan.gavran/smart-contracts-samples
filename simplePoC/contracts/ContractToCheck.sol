pragma solidity ^0.4.4;
import './Checker.sol';

//this is a simple contract illustrating basic communications between contracts and external accounts
contract ContractToCheck 
{
	
	address public checkerAddress;
	int public hypeLevel;
	Checker public  checkerContract;
	
	function ContractToCheck(address checker, int startingLevel) 
	{
		checkerAddress = checker;
		checkerContract = Checker(checker);
		hypeLevel = startingLevel;
	}
	
	function increaseHype(int amount)
	{
		
		hypeLevel += amount;
		checkerContract.hypeIncreased(amount);
		if (checkerContract.isItOK() == false)
			{
			revert();
			}
		
	}
	
		
}
