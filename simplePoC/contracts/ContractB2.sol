pragma solidity ^0.4.4;
import './ContractB1.sol';
//this is a simple contract illustrating basic communications between contracts and external accounts
contract ContractB2 
{
	event DefaultFunctionExecuted();
	event B2ReceivedSomeMoney();
	address bananaBrother;
	
	function ContractB2(address brotherAddress)
	{
		bananaBrother = brotherAddress;
	}
	
	
	function startUpdatesUsingTransfer()
	{
		// this level of code doesn't return exception if it happens, rather - false
		bananaBrother.call(bytes4(sha3("updateUsingTransfer()")));
			
		
		// this would return exception immediately if it happens
//		ContractB1 bananaBrotherContract = ContractB1(bananaBrother);
//		bananaBrotherContract.updateUsingTransfer();
	}
	
	function startUpdatesUsingCall()
	{
		bananaBrother.call(bytes4(sha3("updateUsingCall()")));
	}
	
	function deposit() payable
	{
		B2ReceivedSomeMoney();
	}
	//this didn't get executed at all
	function() payable
	{
		DefaultFunctionExecuted();
		revert();
	}
	
}
