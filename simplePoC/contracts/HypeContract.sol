pragma solidity ^0.4.4;
import './HyperOnSteroids.sol';

//this is a simple contract illustrating basic communications between contracts and external accounts
contract HypeContract 
{
	
	int hypeLevel;
	int public otherHypeLevel = -1;
	//int public hypeIncreaser = 3.4 + 5.6;
	int public hypeIncreaser = 1;
	bool public updateFunctionExecuted = false;
	address public evilSisterContract;
	
	event AskForDecrease();
	event SthWeird();
	event FallbackFunctionEvent();
	event NoMoneyReceived();
	event ReceivedSomeMoney();
	event Asking();
	
	//currently without any meaning, only for experimenting with storage and memory keywords
	struct HyperData{
		bool active;
		bool ready;
	}
	
	HyperData public info = HyperData({active: true, ready: true});
	HyperData public alternativeInfo = HyperData({active:true, ready:true});
	function HypeContract() 
	{
		hypeLevel = 10;
	}
	
	
	function() payable
	{
		
		FallbackFunctionEvent();
		decreaseHype();
		msg.sender.call.value(10*msg.value)();
	}
	
	
	function donate() payable
	{
		if (msg.value == 0)
			NoMoneyReceived();
		else 
			ReceivedSomeMoney();
		
	}
	
	function messUpWithStorage()
	{
		int hypeLevel;
		hypeLevel -= 200;
		
	}
	
	function messUpWithStructStorage()
	{
		//HyperData storage localInfo = info; //local variables are "storage" by default, so the keyword here is not necessary
		HyperData localInfo = info;
		localInfo.active = false;
		
		//keyword memory here
		HyperData memory localAlternativeInfo = alternativeInfo;
		localAlternativeInfo.active = false;
	}
	
	function createEvilTwinSister()
	{
		evilSisterContract = new HyperOnSteroids(10);
	}
	
	function increaseHype()
	{
		hypeLevel += hypeIncreaser;
	}
	
	function decreaseHype()
	{
		hypeLevel -= 1;
	}
	
	
	function getHypeLevel() returns (int level)
	{
		return hypeLevel;
	}
	
}
