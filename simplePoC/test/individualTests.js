var HypeContract = artifacts.require("./HypeContract.sol");
var AntiHypeContract = artifacts.require("./AntiHypeContract.sol");


contract('HypeContract', function(accounts) {
  
   // check whether the contract upon deployment has the right hype level
  it("testing the initial hype level", function() {
    return HypeContract.deployed().then(function(instance) {
      return instance.getHypeLevel.call();
    }).then(function(level) {
      assert.equal(level.valueOf(), 10, "10 wasn't the initial hype level");
    });
  });
  
});
