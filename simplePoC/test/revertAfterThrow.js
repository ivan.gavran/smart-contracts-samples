
var ContractB1 = artifacts.require("./ContractB1.sol");
var ContractB2 = artifacts.require("./ContractB2.sol");

contract('Bananas', function(accounts) {
  
 
  
  it("updates using transfer (exception is propagated)", function() {
	    return ContractB1.deployed()
	    .then(function(instanceB1){
	    	return instanceB1.deposit({from:accounts[0], to: instanceB1.address, value: web3.toWei(4, "ether")})
	    	.then (function(){
	    		console.log("initial ether at B1 contract: "+web3.fromWei(web3.eth.getBalance(instanceB1.address), 'ether').toNumber());
		    	return instanceB1.updateFunctionExecuted.call().then(function(updateExecuted){
	    			console.log("update Executed: "+updateExecuted);
	    		})
	    		.then(function(){
			    	return ContractB2.new(instanceB1.address).then(function(instanceB2){
			    		return instanceB2.deposit({from:accounts[0], to: instanceB1.address, value: web3.toWei(4, "ether")})
		    			.then(function(){
		    				console.log("initial ether at B1 contract: "+web3.fromWei(web3.eth.getBalance(instanceB1.address), 'ether').toNumber());
		    				return instanceB2.startUpdatesUsingTransfer().then(function(){
			    			
				    			return instanceB1.updateFunctionExecuted.call()
				    			.then(function(updateExecuted){
				    				console.log("update Executed: "+updateExecuted);
				    				assert.equal(updateExecuted, false, "update wasn't reverted to false");
				    			});
			    			});
			    		});
		    		});
	    		});
    		});
	    });
  });
  
  it("updates using call (exception is *not* propagated)", function() {
	    return ContractB1.deployed()
	    .then(function(instanceB1){
	    	return instanceB1.deposit({from:accounts[0], to: instanceB1.address, value: web3.toWei(4, "ether")})
	    	.then (function(){
	    		console.log("initial ether at B1 contract: "+web3.fromWei(web3.eth.getBalance(instanceB1.address), 'ether').toNumber());
		    	return instanceB1.updateFunctionExecuted.call().then(function(updateExecuted){
	    			console.log("update Executed: "+updateExecuted);
	    		})
	    		.then(function(){
			    	return ContractB2.new(instanceB1.address).then(function(instanceB2){
			    		return instanceB2.deposit({from:accounts[0], to: instanceB1.address, value: web3.toWei(4, "ether")})
		    			.then(function(){
		    				console.log("initial ether at B1 contract: "+web3.fromWei(web3.eth.getBalance(instanceB1.address), 'ether').toNumber());
		    				return instanceB2.startUpdatesUsingCall().then(function(){
			    			
				    			return instanceB1.updateFunctionExecuted.call()
				    			.then(function(updateExecuted){
				    				console.log("update Executed: "+updateExecuted);
				    				assert.equal(updateExecuted, true, "update wasn't true");
				    			});
			    			});
			    		});
		    		});
	    		});
  		});
	    });
});
  
});
