pragma solidity ^0.4.2;

import "truffle/Assert.sol";
import "truffle/DeployedAddresses.sol";
import "../contracts/MetaCoin.sol";

contract TestMetacoin {

  function testInitialBalanceUsingDeployedContractMacka() {
    MetaCoin meta = MetaCoin(DeployedAddresses.MetaCoin());

    uint expected = 100;

    Assert.equal(meta.getBalance(tx.origin), expected, "Macka Owner should have 100 MetaCoin initially");
  }

  function testInitialBalanceWithNewMetaCoin() {
    MetaCoin meta = new MetaCoin();

    uint expected = 100;

    Assert.equal(meta.getBalance(tx.origin), expected, "Pas Owner should have 100 MetaCoin initially");
  }

}
