# Setup

(last update: January 2018. Since the area is new, tools and dependancies are changing quickly)
 - install [truffle](https://github.com/trufflesuite/truffle)
   - have to use node version 6 ([link](https://github.com/trufflesuite/ganache-cli/issues/216)), even though truffle website suggests using 5.0 should be enough  
 - install [ganache-cli](https://github.com/trufflesuite/ganache-cli) (former [testrpc](https://github.com/ethereumjs/testrpc))
   - for other clients check [this list](http://truffleframework.com/docs/getting_started/client)
 
 - run ganache-cli (with, for example, `ganache-cli -a 4`, you can run a network with four accounts generated at startup)
 - move to directory `smart-contracts-samples/MetaCoin/test` and run `truffle test metacoin.js` 
   - to specify a network to work on, use option --network
   - networks should be defined in `truffle.js`, as suggested [here](https://ethereum.stackexchange.com/questions/30017/truffle-migrate-doesnt-work-error-no-network-specified-cannot-determine-curr). 
   that is particularly useful when using ganache (port 7545) and ganache-cli (port 8545).