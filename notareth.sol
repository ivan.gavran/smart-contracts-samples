// if I register a (hash of a) document first, it would be accepted and nobody else will be able to register it 
// if I am a registered owner, I can transfer a document to whomever I wish

contract Etherdoc {
    address owner;
    
    function Etherdoc(){
        owner = msg.sender; 
    }
    
    // In case you sent funds by accident
	// this is very weird... who protects this function from being invoked by anybody?
	// it would send current balance to the owner. that's a bit confusing
	// also, from the version Solidity v0.4.0, if there is no fallback function, no money can be received
    function empty(){
     uint256 balance = address(this).balance;
     address(owner).send(balance);
    }
    

	//hashing is done client-side so this function accepts the hash and only takes care of whether or not such hash was already seen
    function newDocument(bytes32 hash) returns (bool success){
        if (documentExists(hash)) {
            success = false;
        }else{

            createHistory(hash, msg.sender, msg.sender);
            usedHashes[hash] = true;
            success = true;
        }
        return success;
    }
    function createHistory (bytes32 hash, address from, address to) internal{
            ++latestDocument;
            documentHashMap[hash] = to;
            usedHashes[hash] = true;
            history[latestDocument] = DocumentTransfer(block.number, hash, from, to);
            DocumentEvent(block.number, hash, from,to);
    }
    
	//transfer the "ownership" of the document to somebody else
    function transferDocument(bytes32 hash, address recipient) returns (bool success){
        success = false;
           
        if (documentExists(hash)){
            if (documentHashMap[hash] == msg.sender){
                createHistory(hash, msg.sender, recipient);
                success = true;
            }
        }
         
        return success;
    }
    
    function documentExists(bytes32 hash) constant returns (bool exists){
        if (usedHashes[hash]) {
            exists = true;
        }else{
            exists= false;
        }
        return exists;
    }
    
    function getDocument(uint docId) constant returns (uint blockNumber, bytes32 hash, address from, address to){
        DocumentTransfer doc = history[docId];
        blockNumber = doc.blockNumber;
        hash = doc.hash;
        from = doc.from;
        to = doc.to;
    }
    
    event DocumentEvent(uint blockNumber, bytes32 indexed hash, address indexed from, address indexed to);
    
    struct DocumentTransfer {
        uint blockNumber;
        bytes32 hash;
        address from;
        address to;
    }
    
    function getLatest() constant returns (uint latest){
        return latestDocument;
    }
    
    uint latestDocument;
    
    mapping(uint => DocumentTransfer) public history;
    mapping(bytes32 => bool) public usedHashes;
    mapping(bytes32 => address) public documentHashMap;
}